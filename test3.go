package main

import "fmt"

const N = 10
type ID [N]int
type VALUE []byte
type DATA map[ID]VALUE
var data DATA

func main() {
	var x ID
	var y ID
	for i := 0; i < N; i++ {
		x[i] = i
		fmt.Printf("%d\n", x[i])
		y[i] = x[i]
	} 

	value := data[x]
	fmt.Printf("len and cap of value are %d %d\n", len(value), cap(value))
	value = append(value, 1, 2, 3)
	fmt.Printf("len and cap of value are %d %d\n", len(value), cap(value))

	p := data[x]
	fmt.Printf("len and cap of value are %d %d\n", len(p), cap(p))
	//data[x] = value
	//data[x] = make([]int, len(value))
	//for i := 0; i < len(value); i++ {
	//	data[x][i] = value[i]
	//}

	fmt.Printf("\n");
	for i := 0; i < N; i++ {
		//x[i] = N - i
		fmt.Printf("%d\n", x[i])
	}
}