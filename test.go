package main

import (
    "container/list"
    "fmt"
)

type ID [5]byte

type Kademlia struct {
    IPAddr int
    NodeID ID
}

type Bucket struct {
	x *list.List
	//l list 
}

const B = 160


func main() {
	//s := make([]byte, 5)

	idx := 4

	buckets := make([]Bucket, 5) 
	buckets[idx].x = list.New()


	for i := 0; i < 5; i++ {
		var p byte = byte(i)
	    var id ID = ID{p, p * 2, p * 3}
		x := Kademlia{i,id}
		buckets[idx].x.PushBack(&x)

		fmt.Printf("len = %d\n", buckets[idx].x.Len())
		//y := buckets[idx].x.Remove(buckets[idx].x.Front())
		//fmt.Printf("%d\n", y)
		//fmt.Printf("len = %d\n", buckets[idx].x.Len())
	}

	for e := buckets[idx].x.Front(); e != nil; e = e.Next() {
		var k *Kademlia = e.Value.(*Kademlia)

		fmt.Printf("butterfly len = %d\n", buckets[idx].x.Len())
		fmt.Printf("butterfly %d\n", e.Value)
		if k.IPAddr == 1 {
			y := buckets[idx].x.Remove(e)
			fmt.Printf("remove %d\n", y)

			var id ID = ID{10, 10, 10}
			x := Kademlia{1,id}
			buckets[idx].x.PushBack(&x)
			if e.Next() == nil {
				fmt.Println("test test.\n")
			}
		}
	}

	
	for e := buckets[idx].x.Front(); e != nil; e = e.Next() {
		fmt.Printf("%d\n", e.Value)
	}

}