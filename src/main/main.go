package main

import (
    "flag"
    "fmt"
    "log"
    "math/rand"
    "net"
    "net/http"
    "net/rpc"
    "time"
    "bufio"
    "os"
    "strings"
)

import (
    "kademlia"
)

//var Stdin *bufio.Reader 


func main() {
    // By default, Go seeds its RNG with 1. This would cause every program to
    // generate the same sequence of IDs.
    rand.Seed(time.Now().UnixNano())

    // Get the bind and connect connection strings from command-line arguments.
    flag.Parse()
    args := flag.Args()
    if len(args) != 2 {
        log.Fatal("Must be invoked with exactly two arguments!\n")
    }
    listenStr := args[0]
    firstPeerStr := args[1]

    fmt.Printf("kademlia starting up!\n")
    kadem := kademlia.NewKademlia()

    rpc.Register(kadem)
    rpc.HandleHTTP()
    l, err := net.Listen("tcp", listenStr)
    if err != nil {
        log.Fatal("Listen: ", err)
    }

    // Serve forever.
    go http.Serve(l, nil)

    // Confirm our server is up with a PING request and then exit.
    // Your code should loop forever, reading instructions from stdin and
    // printing their results to stdout. See README.txt for more details.
    client, err := rpc.DialHTTP("tcp", firstPeerStr)
    if err != nil {
        log.Fatal("DialHTTP: ", err)
    }
    ping := new(kademlia.Ping)
    ping.MsgID = kademlia.NewRandomID()
    var pong kademlia.Pong
    err = client.Call("Kademlia.Ping", ping, &pong)
    if err != nil {
        log.Fatal("Call: ", err)
    }
    
    log.Printf("ping msgID: %s\n", ping.MsgID.AsString())
    log.Printf("pong msgID: %s\n", pong.MsgID.AsString())

    //Stdin = bufio.NewReader(os.Stdin) 

    //infinite arg loop
    for {
        Log("Say a command: \n")
        var s string
        fmt.Scan(& s) 
        fmt.Printf("You have entered %s.\n", s) 
        //flag.Parse()
        stuff:= strings.Split(s, " ")//flag.Args()
        if stuff[0] ==  "whoami"{
            //print whoami stuff
            fmt.Printf("NodeID: %v\n", kadem.NodeID)
        } else if stuff[0] ==  "local_find_value" {
            //local lookup for given key.  Print it if it's there.  otherwise, print "ERR"

            if stuff[1] != nil {
                key := ID(stuff[1])
                if kadem.stored(key) {
                    log.Printf("%v\n", kadem.data[key])
                }
                else log.Printf("ERR\n")
            }
            else log.Printf("ERR\n")

       }
        else if stuff[0] == "get_contact" {
            //check if buckets contain given ID, printf("%v %v\n", theNode.addr, theNode.port)
            //otherwise print "ERR"
            for i :=0; i< B; i++ {
                contacts := kadem.buckets[i].contacts
                for n := contacts.Front(); n != nil; n = n.Next(){
                    if stuff[1] != nil {
                        key := ID(stuff[1])
                        if n.NodeID == key {
                            log.Printf("%v %v\n", n.Host, n.Port)
                            break
                        }
                    }
                    else log.Printf("ERR\n")
                }
            }
            log.Printf("ERR\n")
        }
        else if stuff[0] == "iterativeStore" {
            //perform the iterativeStore operation then print the ID of the node that performed the final store operation
            if stuff[1] != nil {
                res := kadem.IterativeStore(ID(args[1]))
                log.Printf("%v\n", res)
            }
        }
        else if stuff[0] == "iterativeFindNode" {
            //print a list of the <= k closest nodes and print their IDs.  Collect IDs in a slice and print them.
            if stuf[1] != nil {
                res := new(IterativeFindNodeResult)
                kadem.IterativeFindNode(ID(args[1]), *res)
                for i:=0; i<res.Nodes.length; i++ {
                    log.Printf("%v\n", res.Nodes[i].NodeID)
                }
            }
        }
        else if stuff[0] == "iterativeFindValue" {
        }

			//WE DON'T HAVE THIS ONE TODO TODO TODO
            //printf("%v %v\n", ID, value) where ID returns the node that finally returned the value.  Print "ERR" if no value was found
        } else if stuff[0] == "ping" {
			if Contains(stuff[1], ":") { //it's just a regular ping thing we can use
				cli, e := rpc.DialHTTP("tcp", stuff[1])
			}	
			else {//we need to find the contact 
				for i:=0; i < B; i++ {
					contacts := kadem.buckets[i].contacts
					for j := contacts.Front(); j != nil; j = j.Next() {
						key := ID(stuff[1])
						if j.NodeID == key {
							cli, e := rpc.DialHTTP("tcp", string(j.Host) + ":" + string(j.Port))
						}
					}
				}
			}
			pi := new(Kademlia.Ping)
			pi.MsgID = kademlia.NewRandomID()
			var po kademlia.Pong
			e = rpc.Call("Kademlia.Ping", pi, &po)
			log.Printf("ping ID: %s\n", ping.MsgID.AsString())
			log.Printf("pong ID: %s\n", pong.MsgID.AsString())
			

            //check second argument, perform a ping
        } else if stuff[0] == "store" {
			//store nodeID key value
			storeReq := new(Kademlia.StoreRequest)
			storeReq.MsgID = kademlia.NewRandomID()
			for i:=0; i<B; i++{
				contacts:=kadem.buckets[i].contacts
				for j := contacts.Front(); j!= nil; j = j.Next() {
					key := ID(stuff[1])
					if j.NodeID == key {
						storeReq.Sender = j
						storeReq.Key = ID(stuff[2])
						storeReq.Value = stuff[3]
						res := new(Kademlia.StoreResult)
						kadem.Store(storeReq, &res)
						log.Printf("\n\n")
					}
				}
			}
            //perform a store and print a blank line.
        } else if stuff[0] == "find_node" {
			findReq := new(Kademlia.FindNodeRequest)
			findReq.NodeID = ID(stuff[1])
			findReq.MsgID = kademlia.NewRandomID()
			var res kademlia.FindNodeResult
			kadem.FindNode(findReq, &res)
			for i := 0; i < res.Nodes.length; i++ {
				log.Printf("%v\n", res.Nodes[i].NodeID)
			}

            //perform a find node and print its results a la iterativeFindNode
        } else if stuff[0] == "find_value" {
			valReq := new(Kademlia.FindValueRequest)
			valReq.MsgID = kademlia.NewRandomID()
			valReq.Key = ID(stuff[1])
			var res kademlia.FindValueResult
			kadem.FindValue(valReq, &res)
			if res.Nodes != nil {
				for i:=0; i<res.Nodes.length; i++ {
					log.Printf("%v\n", res.Nodes[i].NodeID)
				}
			}
            //perform a find value.  If it returns nodes, print them as for find_node.  if it returns a value, print it as for iterativeFindValue
        } else {
            log.Printf("Sorry, no command like that exists.  Try again.\n")
        }
    }
}

