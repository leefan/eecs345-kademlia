package kademlia
// Contains definitions mirroring the Kademlia spec. You will need to stick
// strictly to these to be compatible with the reference implementation and
// other groups' code.

//TODO: use container/heap instead of writing our own.
//TODO: Currently a node iteratively stores (key, value) on other nodes once it receives
//TODO: a new (key, value), maybe need to be changed?
//TODO: Expirtation is not taken into consideration.

import ("fmt"
        "time"
        "net"
        "net/rpc"
        "strconv")

type Contact struct {
    NodeID ID
    Host net.IP
    Port uint16
}

//Error interface and handling
func (e *StoreError) Error() string {
    return fmt.Sprintf("Error: %s", e.What)
}

type StoreError struct {
    What string
}

// PING
type Ping struct {
    Sender Contact
    MsgID ID
}

type Pong struct {
    MsgID ID
    Sender Contact
}

func (k *Kademlia) Ping(ping Ping, pong *Pong) error {
    // This one's a freebie.
    pong.MsgID = CopyID(ping.MsgID)
    pong.Sender = Contact{k.NodeID, //TODO 
    k.UpdateContacts(ping.Sender)
    return nil
}


// STORE
type StoreRequest struct {
    Sender Contact
    MsgID ID
    Key ID
    Value []byte
}

type StoreResult struct {
    MsgID ID
    Err error
}

func (k *Kademlia) Store(req StoreRequest, res *StoreResult) error {

    k.UpdateContacts(req.Sender)

    if len(req.Value) <= 0 {
        res.MsgID = k.NodeID
        res.Err = &StoreError{"Length unspecified."}
        return res.Err
    }

    if k.stored[req.Key] {
        res.MsgID = NewRandomID()
        res.Err = nil
        return nil
    }

    v := make([]byte, len(req.Value), len(req.Value))
    copy(v, req.Value)
    k.data[req.Key] = req.Value
    k.stored[req.Key] = true
    res.MsgID = NewRandomID()
    res.Err = nil
    k.iterativeStore(req.Key)

    return nil
}


// FIND_NODE
type FindNodeRequest struct {
    Sender Contact
    MsgID ID
    NodeID ID
}

type FoundNode struct {
    IPAddr string
    Port uint16
    NodeID ID
}

type FindNodeResult struct {
    MsgID ID
    Nodes []FoundNode
    Err error
}

type PriorityQueueNode struct {
    Node FoundNode
    dis int
}

type PriorityQueue struct {
    queue []PriorityQueueNode
}

func (q *PriorityQueue) Insert(n PriorityQueueNode) {
    max := len(q.queue)
    if max == cap(q.queue) {
        newQueue := make([]PriorityQueueNode, (max + 1) * 2)
        copy(newQueue, q.queue)
        q.queue = newQueue
    }

    idx := max
    for ; idx > 1; {
        if q.queue[idx / 2].dis > n.dis {
            q.queue[idx] = q.queue[idx / 2]
        } else {
            break
        }
    }
    q.queue[idx] = n
}

func (q *PriorityQueue) Remove() PriorityQueueNode{
    max := len(q.queue) - 1
    res := q.queue[0]
    q.queue[0] = q.queue[max]
    q.queue = q.queue[0:max - 1]
    idx := 0
    for ; idx < max; {
        left := idx * 2 + 1
        right := idx * 2 + 2
        if q.queue[left].dis < q.queue[idx].dis && q.queue[left].dis <= q.queue[right].dis {
            tmp := q.queue[left]
            q.queue[left] = q.queue[idx]
            q.queue[idx] = tmp
        } else {
            if q.queue[right].dis < q.queue[idx].dis && q.queue[right].dis <= q.queue[left].dis {
                tmp := q.queue[right]
                q.queue[right] = q.queue[idx]
                q.queue[idx] = tmp
            } else {
                break
            }
        }
    }
    return res
}

func (q *PriorityQueue) Empty() bool{
    if len(q.queue) == 0 {
        return true
    } 
    return false
}

func (k *Kademlia) FindNode(req FindNodeRequest, res *FindNodeResult) error {
    // Used a priority queue to retrieve the nearest node.

    q := new(PriorityQueue)

    for i := 0; i < B; i++ {
            contacts := k.buckets[i].contacts
            for e := contacts.Front(); e != nil; e = e.Next() {

            var c *FoundNode = e.Value.(*FoundNode)

            dis := B - (c.NodeID.Xor(req.NodeID)).PrefixLen()
            if dis == 0 {
                continue
            }

            n := PriorityQueueNode{*c, dis}
            q.Insert(n)
        }

    }

    for i := 0; i < Max_Contacts; i++ {
        res.Nodes[i] = q.Remove().Node
        res.MsgID = NewRandomID()
        res.Err = nil
    }
    return nil
}

type iterativeFindNodeResult struct {
    Nodes [Max_Contacts]FoundNode
}

func (k *Kademlia) iterativeFindNode(Key ID, res *iterativeFindNodeResult) {
    //Iteratively Find nodes.
    candidates := new(PriorityQueue)

    for b := 0; b < B; b++ {
        contacts := k.buckets[b].contacts
        for e := contacts.Front(); e != nil; e = e.Next() {

            var contact *Contact = e.Value.(*Contact)
            var c *FoundNode
            k.Contact2FoundNode(*contact, c)

            dis := B - (c.NodeID.Xor(Key)).PrefixLen()
            if dis == 0 {
                continue
            }

            n := PriorityQueueNode{*c, dis}
            candidates.Insert(n)
        }

    }

    idx := 0
    for ; idx < Max_Contacts; {

        reqs := make([]FindNodeRequest, Alpha)
        nodes := make([]FoundNode, Alpha)
        response := make([]FindNodeResult, Alpha)  

        t := 0  

        for i := 0; i < Alpha; i++ {

            if candidates.Empty() {
                break
            }

            first := candidates.Remove()

            flag := false

            if i > 0 {
                for j := 0; j < i; j++ {
                    if first.Node.NodeID.Compare(reqs[j].NodeID) == 0 {
                        flag = true
                        break
                    }
                }
            }

            if flag {
                continue
            }

            for i := 0; i < len(res.Nodes); i++ {
                if res.Nodes[i].NodeID.Compare(first.Node.NodeID) == 0 {
                    flag = true
                    break
                }
            }

            if flag {
                continue
            }

            t += 1
            //reqs[i] = new(FindNodeRequest)
            reqs[i].MsgID = NewRandomID()
            reqs[i].NodeID = CopyID(Key)
            nodes[i] = first.Node
            
        }

        for i := 0; i < t; i++ {
            client, _ := rpc.DialHTTP("tcp", nodes[i].IPAddr + ":" + strconv.Itoa(int(nodes[i].Port)))

            go client.Call("Kademlia.FindNode", reqs[i], &response[i])
        }

        time.Sleep(1e9)

        for i := 0; i < t; i++ {
            if response[i].Err == nil {
                for j := 0; j < len(response[i].Nodes); j++ {
                    dis := B - (Key.Xor(response[i].Nodes[j].NodeID)).PrefixLen()
                    if dis == 0 {
                        continue
                    }

                    n := PriorityQueueNode{response[i].Nodes[j], dis}
                    candidates.Insert(n)

                    var c Contact
                    k.FoundNode2Contact(response[i].Nodes[j], &c)
                    k.UpdateContacts(c)
                }

                res.Nodes[idx] = nodes[i]
                idx ++
            }
        }

        if candidates.Empty() {
            break
        }

    }

}

const tReplicate = 3.6e12
const tRepublish = tReplicate * 24

func (k *Kademlia) iterativeStore(Key ID) ID  {
    var list iterativeFindNodeResult
    k.iterativeFindNode(Key, &list)
    for i := 0; i < len(list.Nodes); i++ {
        n := list.Nodes[i]
        client, err := rpc.DialHTTP("tcp", n.IPAddr + ":" + strconv.Itoa(int(n.Port)))
        if err != nil {
            continue
        }
        req := new(StoreRequest)
        req.MsgID = NewRandomID()
        req.Key = CopyID(Key)
        copy(req.Value, k.data[Key])
        var res StoreResult
        go client.Call("Kademlia.Ping", req, &res)
    }
    return n.NodeID
}


// FIND_VALUE
type FindValueRequest struct {
    Sender Contact
    MsgID ID
    Key ID
}

// If Value is nil, it should be ignored, and Nodes means the same as in a
// FindNodeResult.
type FindValueResult struct {
    MsgID ID
    Value []byte
    Nodes []FoundNode
    Err error
}

func (k *Kademlia) FindValue(req FindValueRequest, res *FindValueResult) error {

    if k.stored[req.Key] {
        res.MsgID = NewRandomID()
        copy(res.Value, k.data[req.Key])
        res.Err = nil
        res.Nodes = nil
        return nil
    }

    findnode_req := FindNodeRequest{req.Sender, req.MsgID, req.Key}
    var findnode_res FindNodeResult
    k.FindNode(findnode_req, &findnode_res)
    res.MsgID = findnode_res.MsgID
    res.Nodes = findnode_res.Nodes
    res.Err = nil
    return nil
}

// Takes (req FindValueRequest, res *FindValueResult)
// Iteratively calls FindValue for each node found with iterativeFindNode
// Puts result if found into res, otherwise res stays the same way it was passed in
func (k *Kademlia) iterativeFindValue(req FindValueRequest, res *FindValueResult) {
    var list iterativeFindNodeResult
    k.iterativeFindNode(Key, &list)
    for i:= 0; i < len(list.Nodes); i++ {
        n := list.Nodes[i]
        client, err := rpc.DialHTTP("tcp", n.IPAddr + ":" + strconv.Itoa(int(n.Port)))
	if err != nil {
	    continue
	}
	go client.Call("Kademlia.FindValue", req, &res)
    }
}

func (k *Kademlia) FoundNode2Contact(f FoundNode, c *Contact) {
    c.NodeID = CopyID(f.NodeID)
    c.Port = f.Port
    c.Host = net.ParseIP(f.IPAddr)
}

func (k *Kademlia) Contact2FoundNode(c Contact, f *FoundNode) {
    f.NodeID = CopyID(c.NodeID)
    f.Port = c.Port
    f.IPAddr = c.Host.String()
}
