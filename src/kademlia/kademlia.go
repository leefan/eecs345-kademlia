package kademlia
import ("container/list"
	//"fmt"
	"strconv"
	"net/rpc"
        "time")
// Contains the core kademlia type. In addition to core state, this type serves
// as a receiver for the RPC methods, which is required by that package.

// Core Kademlia type. You can put whatever state you want in this.

const Alpha = 3
const B = 160
const Max_Contacts = 20
const timeout_time = 200

type Kademlia struct {
    NodeID ID
    Self Contact
    Buckets [B]Bucket
    Data map[ID][]byte
    Stored map[ID]bool
}

type Bucket struct {
	contacts *list.List
}

func (k *Kademlia) UpdateContacts(newContact Contact) {
	b := B - (k.NodeID.Xor(newContact.NodeID)).PrefixLen()

	//Disregard itself
	if b == B {
		return
	}

	//fmt.Println(b)

	contacts := k.buckets[b].contacts

	//If the contact already exists, it is moved to the end of the bucket.
	for e := contacts.Front(); e != nil; e = e.Next() {

		var c *Contact = e.Value.(*Contact)
		if newContact.NodeID.Compare(c.NodeID) == 0 {
			contacts.Remove(e)
			contacts.PushBack(&newContact)
			return
		}
	}

	if contacts.Len() < Max_Contacts {
		//If the bucket is not full, the new contact is added at the end.
		contacts.PushBack(&newContact)
	} else {
		//If the bucket is full, the node pings the contact at the head of the bucket's
		//list. If that least recently seen contact fails to respond in 
		//an (unspecified) reasonable time, it is dropped from the list, and the
		//new contact is added at the tail. Otherwise the new contact is ignored 
		//for bucket updating purposes.

		//TODO: This thread blocks while wating for the contact to respond.
		//How long do we have to wait for the contact respond until we 
		//stop waiting?
		//Tubes: Fixed this.  Used a select.  After timeout_time seconds, we ignore it and move on.
		ignore := false

		c := contacts.Front().Value.(*Contact)
		client, err := rpc.DialHTTP("tcp", c.Host.String() + ":" + strconv.Itoa(int(c.Port)))
		//time.Sleep(timeout_time * time.Millisecond)

	        if err != nil {
			ignore = true
		}
		if !ignore {
			ping := new(Ping)
			ping.MsgID = NewRandomID()
			var pong Pong
			ch := make (chan error, 1)
			go func() { //put Call in a goroutine
				ch <- client.Call("Kademlia.Ping", ping, &pong)
			}()
			select {
				case err:= <-ch: //if error is something
					if err != nil { //who cares, it's in the contact list and it responded
					//continue
					}
				case <-time.After(timeout_time): //otherwise, remove it
					contacts.Remove(contacts.Front())
					contacts.PushBack(&newContact)
			}
		}
	}
}

func (k *Kademlia) Join(c Contact) {
	k.UpdateContacts(c)
	var res iterativeFindNodeResult
	k.iterativeFindNode(k.NodeID, &res)
	for i := 0; i < len(res.Nodes); i++ {
		var c Contact
		k.FoundNode2Contact(res.Nodes[i], &c)
		k.UpdateContacts(c)
	}
}

func NewKademlia() *Kademlia {
	k := new(Kademlia);

	k.data = make(map[ID][]byte)
	k.stored = make(map[ID]bool)
	k.NodeID = NewRandomID()

	for i := 0; i < B; i++ {
		k.buckets[i].contacts = list.New()
	}
    return k
}



